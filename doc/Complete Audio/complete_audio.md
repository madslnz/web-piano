# Fehlende Piano Sounds vervollständigen

Fehlende Noten:
  85 - 104

  ```
[84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104]
  ```

Idee: Aufnahme mit Sonic Pi

Zuerst nur die fehlenden.
Feststellung, dass die Lautstärke sich nicht so einfach anpassen lässt.

## Lösung

Alle Noten mit Sonic Pi in einem WAV aufnehmen mit jeweils 1 sekunde vorher und nachher Pause.

Danach Analyse über

- `wavplot.py`
- `wavcsv.py`

und Endbearbeitung mit

- `wavslice.py`
- `sortout.py` um Fehler beim erkennen der Töne zu korrigieren
