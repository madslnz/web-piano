import os
import re

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("sourcedir", type=str, default=".", help="Eingabeverzeichnis")
parser.add_argument("bads", type=int, nargs="+", help="Auszusortierende Dateien")
args = parser.parse_args()

bads = args.bads
dirp = os.path.abspath(args.sourcedir)
dirc = os.listdir(dirp)
wavs = list(sorted(dirc, key=lambda v: int(os.path.splitext(v)[0])))

first, last = tuple(map([wavs[0], wavs[-1]], lambda v: int(os.path.splitext(v)[0])))
print("range is from {} to {}".format(first, last))
notes = list(range(first, last))
for wav in wavs:
    wavpath = os.path.join(dirp, wav)
    print("were at {}".format(wavpath))
    o_note = int(os.path.splitext(wav)[0])
    if o_note in bads:
        # os.remove(wavpath)
        print("removing {}".format(wavpath))
        os.remove(wavpath)
    else:
        offset = len(list(filter(lambda v: v < o_note, bads)))
        if offset > 0:
            print("offset is {}".format(offset))
            realnote = o_note - offset
            newname = "{}.wav".format(realnote)
            newpath = os.path.join(dirp, newname)
            print("newname {}".format(newpath))
            os.replace(wavpath, newpath)
        else:
            print("continuing...")
