"""
Skript zum erzeugen einer CSV-Datei, welche die Amplituden innerhalb einer WAV Datei darstellt.
"""

import os
from scipy.io import wavfile
import numpy as np

# Aufrufargumente...
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("wavfile", type=str, help="Input File im WAVE Format")
parser.add_argument("csvpath", type=str, default=".", help="Zielname")
args = parser.parse_args()

# Lade das Sample und ermittle die Samplerate
samplerate, data = wavfile.read(args.wavfile)
times = np.arange(len(data))/float(samplerate)
chan1 = data[:,0]
chan2 = data[:,1]
lines = []
for i in range(len(times)):
    rec = [times[i], chan1[i], chan2[i]]
    srec = list(map(lambda v: str(v), rec))
    recs = ";".join(srec)
    lines.append(recs)

lines.insert(0, "Time;Channel1;Channel2")

linestring = "\n".join(lines)

with open(args.csvpath, "w", encoding="utf8") as outfile:
    outfile.write(linestring)
