"""
Zerteile die WAV in die benötigten Abschnitte.
"""
import os
from scipy.io import wavfile
# from matplotlib import pyplot as plt
import numpy as np

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("wavfile", type=str, help="Input File im WAVE Format")
parser.add_argument("destination", type=str, default=".", help="Zielverzeichnis")
parser.add_argument("-m", "--midistart", type=int, default=21, help="Midi Start Note")

args = parser.parse_args()

filepath = r"C:\Projekte\web-piano\res\sonic_pi_midi_85-104.wav"

# Load the data and calculate the time of each sample
samplerate, data = wavfile.read(args.wavfile)
times = np.arange(len(data))/float(samplerate)

chan1 = data[:,0]
chan2 = data[:,1]

lines = []

SEEK_START = "seek_start"
SEEK_END = "seek_end"

state = SEEK_START

zero_count = 0

intervals = []

c_interval = []

z_threshold = 4000

zeros = []

for i in range(len(chan1)):
    f1, f2 = chan1[i], chan2[i]
    if f1 == 0 and f2 == 0:
        zero_count += 1
    if state == SEEK_START:
        if f1 != 0 or f2 != 0:
            state = SEEK_END
            zeros.append(zero_count)
            zero_count = 0
            c_interval.append((f1, f2))
    elif state == SEEK_END:
        c_interval.append((f1, f2))
        if zero_count >= z_threshold:
            c_interval = c_interval[:-zero_count]
            length = len(c_interval)
            c1_data = [item for item, _ in c_interval]
            c2_data = [item for _, item in c_interval]
            ini_buffer1 = np.ndarray(shape=(length, 1), buffer=np.array(c1_data), dtype=np.int16)
            ini_buffer2 = np.ndarray(shape=(length, 1), buffer=np.array(c2_data), dtype=np.int16)
            wav_interval = np.ndarray(shape=(length, 2), dtype=np.int16)
            for i in range(len(ini_buffer1)):
                wav_interval[i][0] = ini_buffer1[i]
                wav_interval[i][1] = ini_buffer2[i]
            intervals.append(wav_interval)
            c_interval = []
            zero_count += 1
            state = SEEK_START
        else:
            if f1 != 0 or f2 != 0:
                zero_count = 0


midi_start = args.midistart

destination = os.path.abspath(args.destination)

for interval in intervals:
    fname = str(midi_start) + ".wav"
    midi_start += 1
    fpath = os.path.join(destination, fname)
    wavfile.write(fpath, samplerate, interval)


# for i in range(len(times)):
#     rec = [times[i], chan1[i], chan2[i]]
#     srec = list(map(lambda v: str(v), rec))
#     recs = ";".join(srec)
#     lines.append(recs)
#
# linestring = "\n".join(lines)
#
# with open("wav.csv", "w", encoding="utf8") as outfile:
#     outfile.write(linestring)

# # Make the plot
# # You can tweak the figsize (width, height) in inches
# plt.figure(figsize=(20, 4))
# plt.fill_between(times, data[:,0], data[:,1], color='k')
# plt.xlim(times[0], times[-1])
# plt.xlabel('time (s)')
# plt.ylabel('amplitude')
# # You can set the format by changing the extension
# # like .pdf, .svg, .eps
# plt.savefig('plot.png', dpi=100)
# plt.show()
