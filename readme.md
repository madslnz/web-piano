# Web Piano

Mit dieser Webapplikation soll es dem Nutzer möglich sein ein Piano über den Browser zu spielen.
Dabei sollen die Notennamen in einem vom Nutzer wählbaren Textformat ausgegeben werden.
Geplant sind außerdem eine Playbackfunktion basierend auf der vom Nutzer generierten Ausgabe.

Orientierung an [bestehendem Web-Piano](http://www.apronus.com/music/flashpiano.htm)

## Anforderungen
- Grafische Oberfläche (webbasiert)
  - Komponenten
    - Piano
      - Menü
        - Vollansicht
        - Oktavenweise (Intervall wählbar)
    - Sequenzielle Anzeige der gespielten Noten mit Bearbeitungsmöglichkeiten
      - Spielzeit
      - Lautstärke
      - Effekte (Echo, Hall)
    - Textbox für Ausgabe
      - Als Datei abspeichern
    - Ausgabe in Notennotation
- Funktionen
  - Klang
    - aufnehmen
    - abspielen
    - modifizieren
      - Spieldauer
      - Lautstärke
      - Effekte
  - Notenbeschreibungstext
    - generieren
    - bearbeiten
  - Notennotation
    - generieren
    - bearbeiten
  - Dokument speichern/herunterladen

## Objekte

- Aufnahme
  - Notenliste
  - Dauer
- Sound
  - Lautstärke
  - Effekte
  - Resource
  - Dauer
- Piano
  - Intervall
  - Noten
- Note
  - Name
  - Oktave
  - Numerischer Name
  - Dauer
  - Beginn
- Effekt
  - Bezeichnung
  - Modulationsfunktion

## Roadmap

1. Piano Tastatur soll bedienbar sein
  - Web-Interface mit Klaviertastatur erstellen
  - Sounds auf Tastendruck abspielbar
2. Gespielte Sequenz soll in Textbox erscheinen
  - Textbox in bestehendem Interface unterbringen
  - Grundfunktionalität des Aufnahme-Objekts herstellen
    - Mitschneiden gespielter Noten
    - Ausgabe in Textformat
