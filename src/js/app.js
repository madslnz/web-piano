
// Default Werte beim Start der App
const DEFAULT_CONFIG = {
  start_midi: 48,
  end_midi: 96
}

/**
  IDs der der UI-Elemente, welche von Komponenten der App benötigt werden
*/

// Klavier
const ID_PIANO = "#piano";
const ID_KEYBOARD = "#piano-keyboard"
// Klavieroptionen
const ID_OCTAVE_START = "#octave-start-selector";
const ID_OCTAVE_END = "#octave-end-selector";
// Keyboard-Feature Optionen
const ID_KEYBOARD_FEATURE = "#keyboard-feature-option-view";

// Ausgabe
const ID_NOTES_OUTPUT = "#output-container"
// Optionen
const ID_OUTPUT_FORMATS = "#output-formats-container";
const ID_OUTPUT_FORMATS_OPTIONS = "#format-spec-menu";
// Presets
const ID_PRESET_SELECTOR = "#preset-format-selector-view";

// Replay
const ID_REPLAY = "#replay";

/**
  Konstruktor.

    Keine weitere Funktionalität

*/
function PianoApp() {}


/**
 * Initialisiere alle Komponenten der App und aktiviere so die Ausführbarkeit.
 */
PianoApp.prototype.run = function () {
  // Klavier
  // UI-Komponenten
  this.keyboard_view = $(ID_KEYBOARD);
  this.startOctaveSelect = $(ID_OCTAVE_START);
  this.endOctaveSelect = $(ID_OCTAVE_END);
  // Setze die Default-Werte bei den Oktavendropdowns
  this.startOctaveSelect.val(DEFAULT_CONFIG.start_midi);
  this.endOctaveSelect.val(DEFAULT_CONFIG.end_midi);
  // Instanziiere nun das Keyboard
  var keyboard = new Keyboard(DEFAULT_CONFIG.start_midi, DEFAULT_CONFIG.end_midi, this.keyboard_view);
  this.keyboard = keyboard;
  keyboard.initialize();
  // Audio-Ausgabe
  var sound_engine = new SoundEngine();
  this.sound_engine = sound_engine;
  // Hook für die Soundausgabe, wenn eine Keyboardtaste gedrückt wurde
  this.keyboard.add_keypressed_handler(this.sound_engine.playSound, this.sound_engine);
  // Zeichne das Klavier, sobald die nötigsten Komponenten aktiv sind
  this.keyboard.render();

  // Ausgabelog
  // UI-Komponenten
  this.audiolog_options_container = $(ID_OUTPUT_FORMATS);
  this.audiolog_options_controls = $(ID_OUTPUT_FORMATS_OPTIONS);
  this.textout_container = $(ID_NOTES_OUTPUT);
  this.preset_selector = $(ID_PRESET_SELECTOR);
  // Options-Control
  this.audiolog_options = new AudioLogOptionsControl(this.audiolog_options_container, this.audiolog_options_controls);
  this.audiolog_options.initialize();
  // Ausgabe Presets
  var preset_selector_view = PresetSelector.render(this.audiolog_options.add_section, this.audiolog_options);
  this.preset_selector.append(preset_selector_view);
  // Textlog
  this.audiolog = new AudioLog(this.textout_container);
  this.keyboard.add_keypressed_handler(this.audiolog.logNote, this.audiolog);
  this.audiolog.render();

  // Replay
  this.replay_container = $(ID_REPLAY);
  this.replay = new Replay(this.replay_container);
  this.replay.initialize();

  // Keyboard Feature
  this.kbi_view = $(ID_KEYBOARD_FEATURE);
  this.piano_view = $(ID_PIANO);
  this.kbi_feature = new KeyboardInteraction(this.kbi_view, this.piano_view, this.keyboard);
  this.kbi_feature.initialize();
}


/**
 * Aktion die beim drücken des Buttons "Audiolog neu laden" ausgeführt wird.
 */
PianoApp.prototype.reloadAudioLog = function () {
  var format_spec = this.audiolog_options.get_spec();
  this.audiolog.setSpec(format_spec);
};


/**
 * Aktion die beim drücken des Buttons "Klavier neu laden" ausgeführt wird.
 */
PianoApp.prototype.reloadPiano = function () {
  let start = this.startOctaveSelect.val();
  let end = this.endOctaveSelect.val();
  this.keyboard.update(start, end);
  this.kbi_feature.update();
};
