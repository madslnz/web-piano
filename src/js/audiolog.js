/**
 * TextLog Klassenkonstruktor.
 *
 * @param  {jQuery-DOM-Element} textbox TextBox, in welcher das Log ausgegeben werden soll.
 * @return {type}         description
 */
function AudioLog(container, formats = null){
  this.container = container;
  this.log = [];
  this.start_time = null;
  this.formats = formats;
}


/**
 * Trage Note in den Log ein.
 *
 * @param  {Object} note Note für den Log
 */
AudioLog.prototype.logNote = function (note) {
  // Wenn noch nicht aufgenommen wird, starte die Aufnahme jetzt
  if(this.start_time == null){
    this.start_time = Date.now();
    note.time = 0;
  } else {
    // Trage die Startzeit der Note relativ zum Aufnahmestart ein
    var timediff = Date.now() - this.start_time;
    note.time = timediff;
    var played_last = this.log[this.log.length - 1];
    var pausetime = note.time - played_last.time;
    played_last.pause = pausetime;
  }
  this.log.push(note);
  this.update();
};


/**
 * Update das Log und den Inhalt der verknüpften TextBox.
 *
 * @param  {Object} note = Gespielte Note, also Record fürs Log
 */
AudioLog.prototype.update = function () {
  // Wenn kein Format gesetzt ist wird das Log einfach als JSON String ausgegeben
  if(this.formats == null){
    var default_name = Formatter.Name();
    var default_box = this.findRightLog(default_name);
    var textbox_content = JSON.stringify(this.log);
    default_box.val(textbox_content);
  } else {
    // Hier erfolgt die Auswertung und Verteilung auf die einzelnen Textboxen.
    var seperatistos = this.formats.filter((elem) => elem.seperateTextbox == true);
    var crowd = this.formats.filter((elem) => elem.seperateTextbox == false);
    // Zuerst die einzelnen Textboxen
    for (var i = 0; i < seperatistos.length; i++){
      var seperatist = seperatistos[i];
      var logname = Formatter.Name(seperatist.name);
      var logbox = this.findRightLog(logname);
      var new_logcontent = Formatter.Output(this.log, seperatist);
      logbox.val(new_logcontent);
    }
    // Dann die Sammelbox
    if (crowd.length > 0){
      var crowdbox_name = Formatter.Name();
      var crowdbox = this.findRightLog(crowdbox_name);
      var crowdbox_content = Formatter.Output(this.log, crowd);
      crowdbox.val(crowdbox_content);
    }
  }
};


/**
 * Finde die richtige Textlog-Sektion anhand des übergebenen Namens.
 *
 * @param  {type} name Namensstring der Textarea
 * @return {type}      $element
 */
AudioLog.prototype.findRightLog = function (name) {
  var result = this.container.find(`textarea[name=${name}]`);
  return result;
};


/**
 * Zeichne die Audiolog-Sektion.
 *
 * Dazu gehören alle Logs, die Initialisierung und das Updaten des Inhalts, falls bereits vorhanden.
 */
AudioLog.prototype.render = function () {
  // Lösche alle vorhandenen Audiolog-Sektionen
  this.container.empty();
  // Erstelle die Logs anhand der vorgegebenen Spezifikation
  if (this.formats == null){
    // Default-Log mit JSON-Format
    var template = AudioLogSection.render();
    var element = $(template);
    this.init_log(element);
    this.container.append(element);
  } else {
    // Sammelbox mit nicht separaten Ausgaben
    var crowd = this.formats.filter((elem) => elem.seperateTextbox == false);
    if (crowd.length > 0){
      var crowdbox_template = AudioLogSection.render();
      var crowdbox = $(crowdbox_template);
      this.init_log(crowdbox);
      this.container.append(crowdbox);
    }
    // Separate Ausgabeboxen
    var seperatistos = this.formats.filter((elem) => elem.seperateTextbox == true);
    for (var i = 0; i< seperatistos.length; i++){
      var seperatist = seperatistos[i];
      var template = AudioLogSection.render(seperatist.name);
      var element = $(template);
      this.init_log(element, seperatist.name);
      this.container.append(element);
    }
  }
  // Inhalt updaten
  this.update();
};


/**
 * Initialisieren der Textlog-Sektion und seiner Komponenten.
 *
 * @param  {jQuery-DOM-element} element     UI-Element (jQuery-Objekt)
 * @param  {type} name = null Name der Sektion
 */
AudioLog.prototype.init_log = function (element, name = null) {
  // Hole den HTML-konformen Namen über welchen später die Handler die richtige Textarea finden
  var realname = (name == null) ? Formatter.Name() : Formatter.Name(name);
  // Registriere Handlerfunktion für die 'Clipboard'-Funktion
  var copy_to_clipboard_button = element.find(`button[name=${NAME_CLIPBOARD_BUTTON}]`);
  copy_to_clipboard_button.bind('click', {section: element, name: realname}, function(eventdata){
    var parent = $(eventdata.data.section);
    var name = eventdata.data.name;
    var text = parent.find(`textarea[name=${name}]`).val();
    var $tmp = $('<input>');
    $("body").append($tmp);
    $tmp.val(text).select();
    document.execCommand("copy");
    $tmp.remove();
  });
  // Registriere Handler für die 'Datei speichern'-Funktion
  var save_as_file_button = element.find(`button[name=${NAME_SAVE_BUTTON}]`);
  save_as_file_button.bind('click', {section: element, name: realname}, function(eventdata){
    // Es wird ein unsichtbares Link-Objekt (a) erzeugt, welches den Inhalt der
    // Textbox als hier künstlich erzeugte Datei enthält und durch den Klick auf
    // den Button aktiviert.
    var parent = $(eventdata.data.section);
    var name = eventdata.data.name;
    var filename = name + ".txt";
    var text = parent.find(`textarea[name=${name}]`).val();
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  });
};


/**
 * Setze neue Ausgabeformate.
 * Beinhaltet ein neuzeichnen der Ausgabesektion.
 *
 * @param  {type} spec description
 * @return {type}      description
 */
AudioLog.prototype.setSpec = function (spec) {
  this.formats = spec;
  this.render();
};

/******************************************************************************

                                    Formatierung

******************************************************************************/

/**
 * Objekt mit Funktionen zum Formatieren von Einheiten und Strings
 */
var Formatter = {
  /**
   * Item - Formatiere die Midi Informationen der Note.
   *
   * @param  {type} note          Midi Informationen
   * @param  {type} format_string Format-String zur Darstellung der Midi Informationen
   * @param  {type} time_format   Zeitformat (s, s.ss)
   * @param  {type} note_format   Notenformat (midi/name)
   * @return {type}               Formatierter String
   */

  Item: function(note, format_string, time_format, note_format){
    var result = format_string;
    var note_string = (note_format == "midi") ? note.midi : note.name;
    result = result.replace("$note", note_string);
    var time = note.time;
    if(time_format == "ss"){
      time = time / 1000;
      time = time.toFixed(3);
    }
    if(note.pause){
      var pause = note.pause;
      if(time_format == "ss"){
        pause = pause / 1000;
        pause = pause.toFixed(3);
      }
      result = result.replace("$pause", pause);
    } else {
      result = result.replace("$pause", "");
    }
    result = result.replace("$time", time);
    return result;
  },
  /**
   * Bringe den Namen der Box in ein HTML-konformes Format, welches für das 'name' Attribut verwendet werden kann.
   *
   * @param  {type} string = "Sammelbox" Normaler Name
   * @return {type}                      formatierter Namensstring
   */

  Name: function(string = "Sammelbox"){
    var result = string.replace(" ", "-");
    result = encodeURI(result);
    result = result + "-log";
    return result;
  },
  /**
   * Sequence - Formatiere eine Sequenz von Log-Items.
   *
   * @param  {type} items Log-Inhalt
   * @param  {type} spec  Spezifikationen für die Formatierung der Items
   * @return {type}       Formatierter String.
   */

  Sequence: function(items, spec){
    var result_items = []
    if (spec.includeName){
      var name_string = spec.namePattern.replace("$name", spec.name);
      if (name_string.length > 0) result_items.push(name_string);
    }
    var formatted_notes = items.map((elem) => Formatter.Item(elem, spec.itemFormat, spec.timeFormat, spec.noteFormat));
    var list_content_string = formatted_notes.join(spec.delimiter);
    var sequence_string = spec.bounds.left + list_content_string + spec.bounds.right;
    result_items.push(sequence_string)
    var result_string = result_items.join("\n");
    return result_string;
  },
  /**
   * Output - Formatiere die Ausgabe
   *
   * @param  {type} items LogInhalt
   * @param  {type} specs Formatspezifikation
   * @return {type}       Formatierter String.
   */

  Output: function(items, specs){
    if(!Array.isArray(specs)){
      specs = [specs];
    }
    var result_items = [];
    for(var i = 0; i < specs.length; i++){
      var spec = specs[i];
      var section = Formatter.Sequence(items, spec);
      result_items.push(section);
    }
    var result_string = result_items.join("\n\n");
    return result_string;
  }
}

/******************************************************************************

                                    Templates

******************************************************************************/

/******************************************************************************
                                    AudioLogSection
******************************************************************************/

const CLASS_NOTES_PLAY_LOG = "notes-play-log";
const NAME_SAVE_BUTTON = "saveButton";
const NAME_CLIPBOARD_BUTTON = "copyToClipboardButton";

/**
 * Textausgabe Sektion in einem Objekt,
 */
var AudioLogSection = {
  /**
   * render - Erzeuge den Template-String für das DOM-Element.
   *
   * @param  {type} name = "Sammelbox" Name der Box
   * @return {type}                    Element-String
   */
  render: function(name = "Sammelbox"){
    var formatted_name = Formatter.Name(name);
    var template = `
      <div class="container output-section-row">
        <div class="row">
          <div class="col">
            <h3>${name}</h3>
          </div>
          <div class="col">
            <button class="btn" name="saveButton">Speichern</button>
            <button class="btn" name="copyToClipboardButton">Copy to Clipboard</button>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <textarea class="form-control notes-play-log" name="${formatted_name}" rows="10"></textarea>
            </div>
          </div>
        </div>
      </div>
    `;
    return template;
  }
}

/******************************************************************************
                                    ConfigView
******************************************************************************/

// Namen und Klassen
// Controls
const NAME_NOTE_FORMAT_SELECT = "noteFormatSelect";  // Notenformat
const NAME_TIME_FORMAT_SELECT = "timeFormatSelect";  // Zeitformat
const CLASS_DELIMITER_FORMAT_SELECT = "delimiter-select";  // Trennzeichen
const NAME_DELIMITER_INPUT = "customDelimiterInput";  // Trennzeichen: Nutzereingabe
const NAME_LEFT_INPUT = "leftBoundInput";  // Linker Rand
const NAME_RIGHT_INPUT = "rightBoundInput";  // Rechter Rand
const NAME_ITEMFORMAT_INPUT = "itemFormatInput";  // Item-Format (einzelnes Logitem)
const NAME_NEW_FORMAT_BUTTON = "newFormatButton";  // neues Format anlegen
const NAME_EMPTY_FORMATS_BUTTON = "emptyFormatsButton";  // Formate leeren
// Views
//  Container
const CLASS_FORMAT_SPEC_VIEW = "format-spec-view";  // Format-Spezifikation
const CLASS_FORMAT_SPEC_CONTAINER = "format-spec-container";  // Container mit Formaten
//  Controls
const CLASS_TIME_FORMAT_VIEW = "time-format-select-view";
const CLASS_NOTE_FORMAT_VIEW = "note-format-select-view";
const CLASS_BOUNDS_VIEW = "bounds-input-view";
const CLASS_ITEM_FORMAT_VIEW = "item-format-view";

const NAME_DELETE_FORMAT_BUTTON = "deleteFormatButton";
const NAME_FORMAT_NAME_INPUT = "formatNameInput";

const NAME_SEPERATE_TEXTBOX_CHECKBOX = "seperateTextboxCheckbox";
const NAME_INCLUDE_NAME_CHECKBOX = "includeNameCheckbox";

const NAME_LINEFORMAT_INPUT = "formatNameFormatInput";


/**
 * Template String für die Konfigurationssektion in einem Objekt.
 */
var AudioLogSequenceConfigTemplate = {
  /**
   * Erzeuge den HTML-String der Config-Sektion
   *
   * @return {type}  HTML-String
   */
  render: function(){
    var template = `
    <!-- Format-Setup -->
      <div class="row format-spec-view">
        <div class="container format-spec-container">
          <!-- Format Name -->
          <div class="row format-spec-row">
            <div class="container">
              <div class="row">
                <div class="col form-group">
                  <input class="form-control item-format-textbox"  name="formatNameInput" placeholder="Name"></textarea>
                </div>
                <div class="col">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="seperateTextboxCheckbox">
                    <label class="form-check-label">
                      Separate Textbox
                    </label>
                  </div>
                </div>
                <div class="col">
                  <button class="btn" name="deleteFormatButton">Format löschen</button>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="includeNameCheckbox">
                    <label class="form-check-label">
                      Namen mit ausgeben
                    </label>
                  </div>
                </div>
                <div class="col form-group">
                  <input class="form-control item-format-textbox"  name="formatNameFormatInput" placeholder="Format" disabled></textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="row format-spec-row">
            <!-- Trennzeichen -->
            <div class="col delimiter-select">
              <div class="row">
                <div class="col">
                  <h5>Trennzeichen</h5>
                </div>
              </div>
              <div class="row">
                <div class="col form-group">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="custom">
                    <label class="form-check-label">
                      <input type="text" name="customDelimiterInput" class="form-control" placeholder="zb. ," disabled>
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="newline" checked>
                    <label class="form-check-label">
                      Newline
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="semikolon">
                    <label class="form-check-label">
                      Semikolon ( ; )
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <!-- Zeitformat -->
            <div class="col time-format-select-view">
              <div class="row">
                <div class="col-12">
                  <h5>Zeitformat</h5>
                </div>
              </div>
              <div class="row">
                <div class="col form-group">
                  <select class="form-control" name="timeFormatSelect">
                    <option value="ss">Sekunden</option>
                    <option value="sss">Millisekunden</option>
                  /**
                   *
                   */

                  </select>
                </div>
              </div>
            </div>

            <!-- Notenformat -->
            <div class="col note-format-select-view">
              <div class="row">
                <div class="col-12">
                  <h5>Notenformat</h5>
                </div>
              </div>
              <div class="row">
                <div class="col form-group">
                  <select class="form-control" name="noteFormatSelect">
                    <option value="midi">Midinummer</option>
                    <option value="name">Notenname (Bsp.: C3, Bb5)</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row format-spec-row">
            <!-- Bounds -->
            <div class="col bounds-input-view">
              <div class="row">
                <div class="col">
                  <h5>Bounds</h5>
                </div>
              </div>
              <div class="row">
                <div class="col form-group">
                  <div class="form-row">
                    <div class="col">
                      <input type="text" name="leftBoundInput" class="form-control" placeholder="left">
                    </div>
                    <div class="col input-group">
                      <input type="text" class="form-control" placeholder="Listeninhalt" disabled>
                    </div>
                    <div class="col">
                      <input type="text" name="rightBoundInput" class="form-control" placeholder="right">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Item Formatierung -->
          <div class="row item-format-view">
            <div class="container">
              <div class="row">
                <div class="col">
                  <h5>Item Format</h5>
                </div>
              </div>
              <div class="row">
                <div class="col form-group">
                  <textarea class="form-control item-format-textbox" rows="2" name="itemFormatInput"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
    return template;
  }
}

/**
 * AudioLogOptionsControl - Klasse mit Optionen für die AudioLog-Sektion.
 *
 * @param  {type} container Container für die einzelnen Formate.
 * @param  {type} controls  Panel mit UI-Elementen zur Interaktion mit dieser Klasse
 */
function AudioLogOptionsControl(container, controls) {
  this.container = container;
  this.controls = controls;
}


/**
 * AudioLogOptionsControl.initialize - Erzeuge die notwendigen Bindungen für Events auf den Control Buttons
 *
 * @return {type}  description
 */
AudioLogOptionsControl.prototype.initialize = function () {
  // init buttons in this.controls
  this.controls.find(`button[name=${NAME_EMPTY_FORMATS_BUTTON}]`).bind('click', this, function(event_data){
    event_data.data.container.empty();
  });
  this.controls.find(`button[name=${NAME_NEW_FORMAT_BUTTON}]`).bind('click', this, function(event_data){
    event_data.data.add_section();
  });
};

/**
 * AudioLogOptionsControl.init_section - Initialisiere die notwendigen Event Bindungen auf den interaktiven UI-Elementen des Ausgabeformat-Definitions Bereichs.
 *
 * @param  {jQuery-Element} element       Sektion die Initialisiert werden soll.
 * @param  {object} values = null vordefinierte Werte für bestimmte Controls
 * @return {type}               Initialisiertes Element
 */
AudioLogOptionsControl.prototype.init_section = function (element, values = null) {
  // Custom fields handlers
  var context = this;
  var delimiter_section = element.find(`div.${CLASS_DELIMITER_FORMAT_SELECT}`);
  // Aktionen die ausgeführt werden sollen, wenn die Delimiter-Select Radio-Group angesprochen wird
  delimiter_section.find(`input[type=radio]`).bind('change', function(event_data){
    var that = $(this);
    var selected_value = that.val();
    if(selected_value == "custom"){
      context.container.find(`input[name=${NAME_DELIMITER_INPUT}]`).prop("disabled", false);
    } else {
      context.container.find(`input[name=${NAME_DELIMITER_INPUT}]`).prop("disabled", true);
    }
    var radios = delimiter_section.find(`input[type=radio]`);
    for(var i = 0; i < radios.length; i++){
      var radio = $(radios[i]);
      var val = radio.val();
      if (val != selected_value){
        radio.prop("checked", false);
      }
    }
  });
  // Eventhandler-Logik für die Formatierung des Ausgabetitels
  element.find(`input[name=${NAME_INCLUDE_NAME_CHECKBOX}]`).bind("change", element, function(eventdata){
    var parent = eventdata.data;
    var state = $(this).prop('checked');
    parent.find(`input[name=${NAME_LINEFORMAT_INPUT}]`).prop('disabled', !state);
  });
  // Handler fürs löschen des Ausgabeformats
  element.find(`button[name=${NAME_DELETE_FORMAT_BUTTON}]`).bind("click", element, function(eventdata){
    eventdata.data.remove();
  });
  // Setzen der vordefinierten Werte, wenn diese übergeben wurden
  if (values != null){
    element.find(`input[name=${NAME_FORMAT_NAME_INPUT}]`).val(values.name);
    element.find(`input[name=${NAME_SEPERATE_TEXTBOX_CHECKBOX}]`).prop('checked', values.seperateTextbox);
    element.find(`input[name=${NAME_INCLUDE_NAME_CHECKBOX}]`).prop('checked', values.includeName);
    element.find(`input[name=${NAME_LINEFORMAT_INPUT}]`).val(values.namePattern);
    delimiter_section.find(`input[value=${values.delimiter}]`).prop('checked', true).trigger("change");
    if(values.delimiter == "custom"){
      element.find(`input[name=${NAME_DELIMITER_INPUT}]`).val(values.customDelimiter);
    }
    element.find(`input[name=${NAME_LEFT_INPUT}]`).val(values.bounds.left);
    element.find(`input[name=${NAME_RIGHT_INPUT}]`).val(values.bounds.right);
    element.find(`select[name=${NAME_TIME_FORMAT_SELECT}]`).val(values.timeFormat);
    element.find(`select[name=${NAME_NOTE_FORMAT_SELECT}]`).val(values.noteFormat);
    element.find(`textarea[name=${NAME_ITEMFORMAT_INPUT}]`).val(values.itemFormat);
  }
  return element;
};

/**
 * AudioLogOptionsControl.add_section - Lege eine neue Sektion für die Formatdefinition an.
 *
 * @param  {type} formats = null Voreingestellte Werte für die Sektion
 */
AudioLogOptionsControl.prototype.add_section = function (formats = null) {
  if (formats == null){
    var section_string = AudioLogSequenceConfigTemplate.render();
    var section = $(section_string);
    section = this.init_section(section);
    this.container.append(section);
  } else {
    for(var i = 0; i < formats.length; i++){
      var format = formats[i];
      var section_string = AudioLogSequenceConfigTemplate.render();
      var section = $(section_string);
      section = this.init_section(section, format);
      this.container.append(section);
    }
  }
};

/**
 * AudioLogOptionsControl.get_spec - Hole die konfigurierten Formatdefinitionen aus den existierenden Sektionen.
 *
 * @return {Object}  Objekt mit Schlüssel - Wert Paaren der einzelnen Konfigurationsparameter
 */
AudioLogOptionsControl.prototype.get_spec = function () {
  var format_specs = [];
  var format_views = this.container.find("div." + CLASS_FORMAT_SPEC_VIEW);
  for(var i = 0; i < format_views.length; i++){
    var format_view = $(format_views[i]);
    var spec = {};
    var name = format_view.find(`input[name=${NAME_FORMAT_NAME_INPUT}]`).val();
    spec.name = name;
    var seperate = format_view.find(`input[name=${NAME_SEPERATE_TEXTBOX_CHECKBOX}]`).prop('checked');
    spec.seperateTextbox = seperate;
    var include_name = format_view.find(`input[name=${NAME_INCLUDE_NAME_CHECKBOX}]`).prop('checked');
    spec.includeName = include_name;
    if (include_name){
      var name_pattern = format_view.find(`input[name=${NAME_LINEFORMAT_INPUT}]`).val();
      spec.namePattern = name_pattern;
    }

    var delimiter_selected = format_view.find(`div.${CLASS_DELIMITER_FORMAT_SELECT}`).find(`input[type=radio]:checked`).val();
    if(delimiter_selected == "custom") {
      delimiter_selected = format_view.find(`input[name=${NAME_DELIMITER_INPUT}]`).val();
    } else {
      switch (delimiter_selected) {
        case "semikolon":
          delimiter_selected = ";"
          break;
        case "newline":
          delimiter_selected = "\n";
          break;
        default:
          break;
      }
    }
    spec.delimiter = delimiter_selected;
    spec.bounds = {
      left: format_view.find(`input[name=${NAME_LEFT_INPUT}]`).val(),
      right: format_view.find(`input[name=${NAME_RIGHT_INPUT}]`).val()
    };
    spec.timeFormat = format_view.find(`select[name=${NAME_TIME_FORMAT_SELECT}]`).val();
    spec.noteFormat = format_view.find(`select[name=${NAME_NOTE_FORMAT_SELECT}]`).val();
    spec.itemFormat = format_view.find(`textarea[name=${NAME_ITEMFORMAT_INPUT}]`).val();
    format_specs.push(spec);
  }
  if (format_specs.length == 0){
    return null;
  }
  return format_specs;
};
