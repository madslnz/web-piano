/**
 * Modul: Tastatur-Feature
 *
 * In diesem Modul wird die Funktionalität der Tastaturbedienung implementiert.
 * Vom Nutzer kann die Startoktave, und die Reichweite, sowie abhängig von den
 * beiden Punkten auch die Startposition der Tastaturbindungen wählen.
 */

// Aktivierungscheckbox
const NAME_KBI_TOGGLE ="keyboardInteractionToggleCheckbox";
// Klasse des Optionspanels (im Template enthalten)
const CLASS_KBI_OPTIONS = "keyboard-interaction-options";
// Namen der einzelnen Controls für die Keyborad-Bedienung
const NAME_KBI_STARTOCTAVE = "keyboardInteractionStartOctave";  // Startoktave
const NAME_KBI_RANGE = "keyboardInteractionRange";  // Reichweite
const NAME_KBI_STARTKEY = "keyboardInteractionStartKey";  // Starttaste

/**
 * KeyboardInteraction - Klassenkonstruktor
 *
 * @param  {type} container Container der Sektion
 * @param  {type} area      Fokus Bereich, der aktiv sein muss (Cursor innerhalb), damit die Tastaturbedienung angesprochen wird
 * @param  {type} keyboard  Instanz des Klaviers
 */
function KeyboardInteraction(container, area, keyboard){
  this.container = container;
  this.area = area;
  this.keyboard = keyboard;
  this.capture = false;
  this.map = {};
};

/**
 * KeyboardInteraction.initialize - Initialisiere die Event-Handler der UI-Elemente
 */
KeyboardInteraction.prototype.initialize = function () {
  // Handler für Toggle-Funktion
  // Zeige die Optionen nur dann wirklich an, wenn der Haken im Kästchen gesetzt ist
  this.container.find(`input[name=${NAME_KBI_TOGGLE}]`).bind('change', this, function(eventdata){
    var context = eventdata.data;
    var test = $(this).is(':checked');
    if(test){
      var template = KeyboardInteractionOptions.render();
      var element = $(template);
      KeyboardInteractionOptions.init(element);
      context.container.append(element);
    } else {
      var section = context.container.find(`div.${CLASS_KBI_OPTIONS}`).remove();
    }
  });
  // Mouse-Enter auf der Fokus-Region registrieren
  this.area.mouseenter(function(){
    $(this).addClass('piano-focus');
  });
  // Mouse-Leave auf der Fokus-Region registrieren
  this.area.mouseleave(function(){
    $(this).removeClass('piano-focus');
  });
  // Keypress-Handler in der Fokusregion registrieren
  var $area = this.area;
  var _this = this;  // Als Kontext innerhalb der delegierten Funktion
  document.addEventListener('keypress', (event) => {
    // Nur, wenn der Fokus mit dem Mauszeiger innerhalb der Area liegt
    if($area.hasClass('piano-focus')){
      if(_this.capture){
        var key = event.key;
        var midi_info = _this.map[key];
        if(midi_info != undefined){
          _this.keyboard.keyPressed({data: midi_info});
        }
      }
    }
  });
};

/**
 * KeyboardInteraction.update - Aktualisiere den aktuellen Status mit den aktuellen Konfigurationswerten.
 *
 */
KeyboardInteraction.prototype.update = function () {
  // Hole die Optionen-Sektion
  var options = this.container.find(`div.${CLASS_KBI_OPTIONS}`);
  // Keine angezeigt -> deaktiviere die Funktion
  if (options.length == 0){
    this.capture = false;
    this.map = {};
    return;
  }
  // Hole die Werte der Konfiguration
  var settings = KeyboardInteractionOptions.get_values(options);
  if (settings == null){
    this.capture = false;
    this.map = {};
  } else {
    // Und aktiviere sie, wenn sie existieren
    this.capture = true;
    var end = settings.start + (12 * settings.range);
    var notes = Midi.Notes(settings.start, end);
    var map = {};
    if (settings.range == 1){
      map = this.map_octave(notes, settings.key);
    } else {
      map = this.map_octave(notes, settings.key);
      second = this.map_octave(notes, 'Y', 12);
      map = Object.assign({}, map, second);
    }
    this.map = map;
  }
};

// Stütze für die Tastaturbelegung (Aufbau der PC-Tastatur)
const KEYBOARD_MAP = {
  0: ["2","3","4","5","6","7","8","9","0","ß"],
  1: ["q","w","e","r","t","z","u","i","o","p","ü"],
  2: ["a","s","d","f","g","h","j","k","l","ö","ä"],
  3: ["y","x","c","v","b","n","m",",",".","-"]
}

/**
 * KeyboardInteraction.map_octave - Erzeuge die Map für eine Oktave der Noten-Tastaturbelegung
 *
 * @param  {type} notes          Noten, die gemappt werden sollen
 * @param  {type} startkey       Starttaste
 * @param  {type} startindex = 0 Startindex
 * @return {Objekt}              Map mit der Zuordnung Note zu Taste
 */
KeyboardInteraction.prototype.map_octave = function (notes, startkey, startindex = 0) {
  var white_line = null;
  var black_line = null;
  var black_offset = 0;
  switch (startkey) {
    case 'Q':
      white_line = KEYBOARD_MAP['1'];
      black_line = KEYBOARD_MAP['0'];
      break;
    case 'A':
      white_line = KEYBOARD_MAP['2'];
      black_line = KEYBOARD_MAP['1'];
      black_offset = 1;
      break;
    case 'Y':
      white_line = KEYBOARD_MAP['3'];
      black_line = KEYBOARD_MAP['2'];
      black_offset = 1;
      break;
    default:
      break;
  }
  var white_index = 0;
  var result = {};
  for(var i = 0; i <= 12; i++){
    var note = notes[i + startindex];
    var key = null;
    if (note.sign == ''){
      key = white_line[white_index];
      white_index++;
    } else {
      key = black_line[black_offset + (white_index - 1)];
    }
    if (key != null){
      result[key] = note;
    } else {
      console.log("howdy");
    }
  }
  return result;
};

/**
 * Optionstemplate des Keyboard-Features
 */
var KeyboardInteractionOptions = {
  DEFAULTS: {
    OCTAVES: {
      OPTIONS: [24, 36, 48, 60, 72, 84, 96]
    },
    RANGES: {
      DEFAULT: 2,
      96: 1
    },
    STARTKEYS: {
      RANGE: {
        1: ["Q", "A", "Y"],
        2: ["Q"]
      }
    }
  },
  /**
   * get_values - Holen der gesetzten Werte aus der Optionssektion.
   *
   * @param  {type} container Container mit den Optionselementen
   * @return {type}           Objekt mit Schlüssel-Wert zuordnung der Optionen
   */

  get_values: function(container){
    var keyselect = container.find(`select[name=${NAME_KBI_STARTKEY}]`);
    var octaveselect = container.find(`select[name=${NAME_KBI_STARTOCTAVE}]`);
    var rangeselect = container.find(`select[name=${NAME_KBI_RANGE}]`);
    return {
      key: keyselect.val(),
      start: octaveselect.val(),
      range: rangeselect.val()
    };
  },
  /**
   * init - Initialisiere die Event-Handler der Optionssektion
   *
   * @param  {jQuery-Element} options  Instanz der Optionssektion
   */
  init: function(options){
    options.find(`select[name=${NAME_KBI_RANGE}]`).bind('change', options, function(eventdata){
      var context = eventdata.data;
      var select = $(this);
      var val = select.val();
      var keyselect = context.find(`select[name=${NAME_KBI_STARTKEY}]`);
      keyselect.empty();
      if(val == "2"){
        keyselect.append(`<option value="Q">Q</option>`);
        keyselect.val('Q');
      } else {
        var keys = KeyboardInteractionOptions.DEFAULTS.STARTKEYS.RANGE['1'];
        for (var i = 0; i < keys.length; i++){
          var key = keys[i];
          var key_template = `<option value="${key}">${key}</option>`;
          keyselect.append(key_template);
        }
        keyselect.val('Q');
      }
    });
    options.find(`select[name=${NAME_KBI_STARTOCTAVE}]`).bind(`change`, options, function(eventdata){
      var context = eventdata.data;
      var val = $(this).val();
      var select = context.find(`select[name=${NAME_KBI_RANGE}]`);
      // console.log(val);
      if(val == '96'){
        // console.log();
        context.find(`select[name=${NAME_KBI_RANGE}] option[value=2]`).remove();
        select.val('1');
        select.trigger('change');
        var keyselect = context.find(`select[name=${NAME_KBI_STARTKEY}]`);
        keyselect.empty();
        keyselect.append(`<option value="Q">Q</option>`)
      } else {
        var result = context.find(`select[name=${NAME_KBI_RANGE}] option`);
        if(result.length == 1){
          select.append(`<option value="2">2</option>`)
          select.val('1');
          select.trigger('change');
        }
      }
    });
  },
  /**
   * render - Erzeuge das Template der Optionssektion.
   *
   * @return {type}  HTML-String
   */
  render: function(){
    var template = `
      <div class="row keyboard-interaction-options">
        <div class="col">
          <div class="container">
            <div class="row">
              <div class="col">
                <h5>Startoktave</h5>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <select class="form-control" name="keyboardInteractionStartOctave">
                  <option value="24">1</option>
                  <option value="36">2</option>
                  <option value="48">3</option>
                  <option value="60">4</option>
                  <option value="72">5</option>
                  <option value="84">6</option>
                  <option value="96">7</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="container">
            <div class="row">
              <div class="col">
                <h5>Reichweite</h5>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <select class="form-control" name="keyboardInteractionRange">
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="container">
            <div class="row">
              <div class="col">
                <h5>Tastatur Startposition</h5>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <select class="form-control" name="keyboardInteractionStartKey">
                  <option value="Q">Q</option>
                  <option value="A">A</option>
                  <option value="Y">Y</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
    return template;
  }
}
