/**
 * Modul mit Replay Funktion.
 */

// Namen der interaktiven UI-Elemente der Replay-Funktion
const NAME_REPLAY_BUTTON = "playReplayButton";
const NAME_STOP_BUTTON = "stopReplayButton";
const NAME_REPLAY_CONTENT = "replayContentString";
const NAME_CLEAR_REPLAY_BUTTON = "clearReplayButton";
const NAME_CHOOSE_FILE_BUTTON = "chooseFileButton";
const NAME_FILE_NAME_INPUT = "fileNameInput";

/**
 * Replay - Klassenkonstruktor.
 *
 * @param  {type} container Container, in dem sich der Replay-Bereich befindet.
 */
function Replay(container){
  this.container = container;
  this.data = [];
  this.timers = [];
  this.soundengine = new SoundEngine();  // Erzeuge eigene SoundEngine für die Instanz des Replays
}

/**
 * Replay.initialize - Initialisiere die UI-Elemente des Replay-Bereichs
 *
 * @return {type}  description
 */
Replay.prototype.initialize = function () {
  this.data_input = this.container.find(`textarea[name=${NAME_REPLAY_CONTENT}]`);
  // Play Button
  this.container.find(`button[name=${NAME_REPLAY_BUTTON}]`).bind('click', this, function(eventdata) {
    var context = eventdata.data;
    var replay = context.data_input.val();
    context.setData(replay);
    context.play();
  });
  // Stop Button
  this.container.find(`button[name=${NAME_STOP_BUTTON}]`).bind('click', this, function(eventdata){
    var context = eventdata.data;
    context.stop();
  });
  // Clear Textbox Button
  this.container.find(`button[name=${NAME_CLEAR_REPLAY_BUTTON}]`).bind('click', this, function(eventdata){
    var context = eventdata.data;
    context.container.find(`textarea[name=${NAME_REPLAY_CONTENT}]`).val('');
  });
  // Upload File Button
  this.container.find(`button[name=${NAME_CHOOSE_FILE_BUTTON}]`).bind('click', this, function(eventdata){
    eventdata.data.upload();
  });
  // Anzeige des Dateinamens
  this.container.find(`input[name=${NAME_FILE_NAME_INPUT}]`).bind('change', this, function(eventdata){
    var context = eventdata.data;
    var value = $(this).prop('files')[0];
    var splitname = value.name.split("\\");
    var filename = splitname[splitname.length - 1];
    var reader = new FileReader();
    reader.readAsText(value, 'UTF-8');
    reader.onload = function (evt) {
      context.data_input.val(evt.target.result);
    };
    reader.onerror = function (evt){
      context.data_input.val('error reading file');
    }
    context.container.find(`label[for=${NAME_FILE_NAME_INPUT}]`).text(filename);
  });
};

/**
 * Replay.setData - Übernehme die Midi-Informationen in den replay Buffer
 *
 * @param  {type} data Liste mit Noten-Midiinformationen (quasi Lied)
 * @return {type}      description
 */
Replay.prototype.setData = function (data) {
  if(typeof(data) == "string"){
    data = JSON.parse(data);
  }
  // Zuerst Daten sortieren nach Zeitstempel
  var sorted = data.sort(function(a, b) {
    if (a.time > b.time){
      return 1;
    } else if (a.time == b.time) {
      return 0;
    } else if (a.time < b.time) {
      return -1;
    }
  });
  this.data = sorted;
  this.data_string = JSON.stringify(data);
};

/**
 * Replay.upload - File Upload (~~~ under construction ~~~)
 */
Replay.prototype.upload = function () {
  // var fileinput_template = `<input type="file" name="file" style="display: none;" id="file-choose-mock-button"/>`;
  // var input = document.createElement("input");
  // input.setAttribute('type', 'file');
  // input.setAttribute('name', 'file');
  // input.setAttribute('style', 'display: none;');
  // input.setAttribute('id', 'file-choose-mock-button');
  // input.addEventListener('change', function(eventdata){
  //   var file = eventdata.file;
  //   console.log(file);
  //   document.body.removeChild(input);
  // });
  // var $element = $("#file-choose-mock-button");
  // console.log($element);
  // $element.trigger('click');
  document.body.appendChild(input);
  input.click();
};

/**
 * Replay.play - Spiele den aktuellen replay-Buffer.
 *
 * @return {type}  description
 */
Replay.prototype.play = function () {
  this.timers = [];
  // Prebuffern der benötigten Noten für flüssigere Wiedergabe
  var noterange = this.data.map((el, i, arr) => el.midi).sort((a, b) => (a.midi != b.midi) ? ((a.midi > b.midi) ? -1 : 1) : 0);
  var start = noterange[0].midi;
  var end = noterange[noterange.length - 1].midi;
  this.soundengine.preBuffer(start, end);
  // Spiele die erste Note
  var first_note = this.data[0];
  this.playNote(first_note);
  var replay_start = Date.now();
  // Registriere Timer fürs auslösen jeder weiteren gespielten Note
  for(var i = 1; i < this.data.length; i++){
    var midi_data = this.data[i];
    var i_elapsed = Date.now() - replay_start;
    var timediff = parseInt(midi_data.time) - i_elapsed;
    var timer = setTimeout(function(data){
      var context = data.context;
      var midi_data = context.data[data.index];
      context.playNote(midi_data);
    }, timediff, {context: this, index: i});
    this.timers.push(timer);
  }
};

/**
 * Replay.playNote - Spiele die Note und highlighte sie im Replay-Textfeld
 *
 * @param  {type} midi_data Midi-Notendaten
 * @return {type}           description
 */
Replay.prototype.playNote = function (midi_data) {
  this.soundengine.playSound(midi_data);
  this.highlight(midi_data);
};

/**
 * Replay.stop - Stoppe die aktuelle Wiedergabe
 *
 * @return {type}  description
 */
Replay.prototype.stop = function () {
  for(var i = 0; i < this.timers.length; i++){
    var timer = this.timers[i];
    clearTimeout(timer);
  }
};

/**
 * Replay.highlight - highlighten der Note im Textfeld
 *
 * @param  {type} note Note (nach dem String wird gesucht)
 * @return {type}      description
 */
Replay.prototype.highlight = function (note) {
  var midi_string = JSON.stringify(note);
  console.log(midi_string);
  var index = this.data_string.indexOf(midi_string);
  console.log(index);
  this.data_input.selectRange(index, index + midi_string.length);
};

/**
 * fn - Implementiere jQuery Extension Funktion zum highlighten von Text in einer Textarea.
 *
 * (Quelle: https://gist.github.com/beiyuu/2029907)
 *
 * @param  {type} start Startindex
 * @param  {type} end   Endindex
 */
$.fn.selectRange = function(start, end) {
    var e = document.getElementById($(this).attr('id')); // I don't know why... but $(this) don't want to work today :-/
    if (!e) return;
    else if (e.setSelectionRange) { e.focus(); e.setSelectionRange(start, end); } /* WebKit */
    else if (e.createTextRange) { var range = e.createTextRange(); range.collapse(true); range.moveEnd('character', end); range.moveStart('character', start); range.select(); } /* IE */
    else if (e.selectionStart) { e.selectionStart = start; e.selectionEnd = end; }
};
