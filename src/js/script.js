// App als globale Konstante
const APP = new PianoApp();

// Funktion wird ausgeführt, wenn die Webseite durch den Browser vollständig
// geladen wurde.
// Startet die App, sobald das Fenser fertig dafür ist.
$(document).ready(function() {
  APP.run();
})
