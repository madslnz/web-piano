/**
 * Modul für die Sounderzeugung.
 *
 */

// Pfad zu den Audiodateien der Klaviernoten.
const AUDIO_DATA_PATH = "./res/audio/"

/**
 * SoundEngine - Klassenkonstruktor
 *
 * @param  {type} start = null Startnote (prebuffering)
 * @param  {type} end = null   Endnote (prebuffering)
 * @return {type}              description
 */
function SoundEngine(start = null, end = null) {
  this.soundBuffer = [];
  if (start != null || end != null){
    if (start == null) start = 21;
    if (end == null) end = 108;
    this.preBuffer(start, end);
  }
}

/**
 * SoundEngine.preBuffer - Vorzeitiges Laden der Audiodateien in den Soundbuffer für flüssigeres spielen.
 *
 * @param  {type} start Startnote
 * @param  {type} end   Endnote
 */
SoundEngine.prototype.preBuffer = function (start, end) {
  for(var midi_note = start; midi_note <= end; midi_note++){
    if(!this.soundBuffer[midi_note]){
      var wav_path = AUDIO_DATA_PATH + midi_note + ".wav";
      this.soundBuffer[midi_note] = new Audio(wav_path);
    }
  }
};

/**
 * SoundEngine.playSound - Spiele die Note.
 *
 * @param  {type} note     Midi-Informationen
 * @param  {type} amp=1    Lautstärke (under dev...)
 * @param  {type} mod=null Modulation (Effekte ... under dev...)
 */
SoundEngine.prototype.playSound = function (note, amp=1, mod=null) {
  // Wenn die Note noch nicht im Soundbuffer liegt
  if(this.soundBuffer[note.midi] == undefined){
    // Lade sie
    var wav_path = AUDIO_DATA_PATH + note.midi + ".wav";
    this.soundBuffer[note.midi] = new Audio(wav_path);
  }
  // Kopiere die Note aus dem Soundbuffer
  var sound = this.soundBuffer[note.midi].cloneNode();
  if(sound != null){
    // Spiele die Note
    sound.play();
  } else {
    console.log("Audio play error " + note.midi);
  }
};
